(function($){
  $(document).ready( function(){

    var _nameProject = 'Enjoei', 
        _srcImg, 
        _imgLink = $('[data-js="img-link"]'), 
        _imgSrc = $('[data-js="img-src"]'),  
        _idBlackFriday,
        _couponTitle = $('[data-js="coupon-title"]'),
        _couponDiscount = $('[data-js="coupon-discount"]'), 
        _resumeValue = $('[data-js="resume-value"]'), 
        _resumeCoupon = $('[data-js="resume-coupon"]'), 
        _resumeShippingPrice = $('[data-js="resume-shippingPrice"]'), 
        _resumetotalPrice = $('[data-js="resume-totalPrice"]'), 
        _msgTit = $('[data-js="modal-title"]'),
        _msgMsg = $('[data-js="modal-msg"]'), 
        _btnCancel = $('[data-js="cancel"]'),
        _btnSucess = $('[data-js="sucess"]');


    $.ajax({
        url : "http://localhost:3000/api/checkouts/6544",
        method : 'GET',
    })
    .done(function( response ){

      $('title').html(response.product.title + ' | ' + _nameProject);

      _srcImg = response.product.image;
      _imgLink.attr('href', _srcImg);
      _imgSrc.attr('src', _srcImg);

      _idBlackFriday = response.checkout.availableCoupons[0];
      _couponTitle.html( _idBlackFriday.title );
      _couponDiscount.html( '- R$ ' + _idBlackFriday.discount );

      _resumeValue.html( 'R$ ' + response.product.price );
      _resumeCoupon.html( '- R$' + _idBlackFriday.discount );
      _resumeShippingPrice.html( 'R$' + response.checkout.shippingPrice );
      _resumetotalPrice.html( 'R$ ' + response.checkout.totalPrice );

      
      // console.log( $('#couponNo').prop('checked') );
    })
    .fail( function( jqXHR, textStatus, msg ){
      alert(msg);
    });

    function modalClose() {
      $('#overlay').on( 'click', function(e) {
        e.preventDefault();

        $(this).parents().removeClass('is-active c-modal--cancel c-modal--sucess');
      });
    }
    
    function cancelBuy() {
      _btnCancel.on( 'click', function(e) {
        e.preventDefault();

        $(this).parents().find('.c-modal').addClass('c-modal--cancel');
        $(this).parents().find('.c-modal').toggleClass('is-active');

        $(this).parents().find( _msgTit ).html( 'compra cancelada' );
        $(this).parents().find( _msgMsg ).html( 'o pedido não foi enviado e você não será cobrado' );
      });
    }

    function sucessBuy() {
      _btnSucess.on( 'click', function(e){
        e.preventDefault();

        $.ajax({
          url : "http://localhost:3000/api/checkouts/6544",
          method : 'POST',
        })
        .done(function(response){
          console.log(response);
        })

        $(this).parents().find('.c-modal').addClass('c-modal--sucess');
        $(this).parents().find('.c-modal').toggleClass('is-active');

        $(this).parents().find( _msgTit ).html( 'compra confirmada' );
        $(this).parents().find( _msgMsg ).html( 'enciaremos atualizações sobre o pedido para o seu email' );
      });
    }

    modalClose();
    cancelBuy();
    sucessBuy();
    
  });
})(jQuery)